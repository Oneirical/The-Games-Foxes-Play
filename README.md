# The Games Foxes Play
A nontraditional traditional roguelike about critters and soul harvesting. 

Crumble under dozens of stowaway spectral parasites, sell them for profit to a narcissistic hivemind, use laser beams to defeat hordes of cybernetically enhanced snails, die, reincarnate at the edge of spacetime itself, then do it all over again!

This game is in active development and is nowhere near a complete state. I post weekly on [r/roguelikedev](https://www.reddit.com/r/roguelikedev/) about my progress in the Sharing Saturday threads. A collection of all these posts can be viewed [here](https://github.com/Oneirical/The-Games-Foxes-Play/tree/main/design/Development%20Logs)!

Design goals:

* **Failure is as valuable as success** - Death of the physical body is unimportant - only faith matters. Failing encounters causes a peculiar infection to spread further, unlocking great power at the cost of one's very identity...

* **An unusual alternative to experience points and spellcasting** - Slaying creatures awards you with Souls. Souls can be invoked for minor powers, or fuelled by Axioms to unleash even greater effects, fully customizable with an in-game spell crafting system!

* **Map generation that responds to player interaction** - The dungeons in TGFP are dreamscapes, moulded by those who imagine them. Depending on which Souls you select and where you place them in the Soul Cage, the denizens populating the illusory worlds below can range from fully mechanized snakes to eerily staring cat statues. 

* **Extensive lore presented through nonmodal elements** - Should you ever start wondering if the setting actually makes sense or if I just injected metric tons of illegal psychedelics in my bloodstream to make this game, you can learn all my worldbuilding through flavour text and dialogue, but you will never be locked out of key-mashing and combat. Those who don't care about the "why" and just want to smash some robotic critters can easily do so without interruption. 

I try to keep the latest winnable and potentially fun build on [itch.io](https://oneirical.itch.io/tgfp).

This game is 100% open source and MIT-licensed, because free stuff is cool.