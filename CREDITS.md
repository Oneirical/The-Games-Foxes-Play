Oneirical - mostly everything

# Assets

Zennith - music

# Code, libraries and dependencies

Line interpolation algorithm
Copyright (c) 2023 Amit Patel
Line interpolation algorithm is licensed under the MIT License.
http://www.opensource.org/licenses/mit-license

Broughlike Tutorial
Copyright (c) 2019 Jeremiah Reid
Broughlike Tutorial is licensed under the MIT License.
http://www.opensource.org/licenses/mit-license

A* Algorithm
Copyright (c) Brian Grinstead, http://briangrinstead.com
A* Algorithm is licensed under the MIT License.
http://www.opensource.org/licenses/mit-license

PIXI.js
Copyright (c) 2013-2023 Mathew Groves, Chad Engler
pixi.js is licensed under the MIT License.
http://www.opensource.org/licenses/mit-license

PIXI-Viewpoint
Copyright (c) 2017 YOPEY YOPEY LLC
PIXI-Viewpoint is licensed under the MIT License.
http://www.opensource.org/licenses/mit-license

GreenSock
Copyright (c) 2023 GreenSock
GreenSock is licensed under a custom no-commercial use license.
https://greensock.com/standard-license/

Glitch animation
Copyright (c) 2022 Supremo Web Design
Glitch animation is licensed under the MIT License.
http://www.opensource.org/licenses/mit-license