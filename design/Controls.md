# Controls

I tried to make it so 7 extra arms, 3 tentacles and a few robotic cybernetics are not required to play the game.

w/a/s/d - Move/attack/interact with NPCs in the 4 cardinal directions. There is no diagonal movement in TGFP.

q - Draw Souls from your storage into your hotbar. It's kind of like reloading a gun with multicoloured bullets. It stands for "Quiver". On top of a Soul Cage, it will retrieve the caged soul.

1-8 - Invoke Souls from your wheel. It's kind of like firing said gun. On top of a Soul Cage slot, it will cage the soul instead.  You can also click on the souls to cast them.

l - Open your inventory. You may see the effects of all six basic Soul types, and equip Legends you have crafted.

f - Open the Research menu. This contains a ton of tutorial information sorted in neat pages, as well as various patterns available for crafting.

c - Enter examine mode, to read tile descriptions and creature abilities. It stands for "Cursor". You can then move your mouse around to inspect things!