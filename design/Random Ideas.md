## This is a chaotic canvas on which I launch all the random things I come up with while going about my day and bugs I notice throughout playtesting. It is not meant to be comprehensible.

# next thing: add the new unique datatypes and make assimilate match for datatypes

* Add Form grid (range targeters) images in their descriptions for clarity
* A better ui for descriptions, with definitions for each keyword like 'exhale' or 'targets' or the status effects, StS style

Lifecycle of an Axiom:

Feral nightmares generate chaotic axiom components.
Unhinged hunters capture them and dissect them.
Vile merchants collect and order parts in a treasury and distribute to Artistic.
* An escherian staircase with Felidols floating around, gaining wealth for the Vile
Saintly creates blueprints to follow.
Artistic creates the Axioms from the blueprints.
Ordered collect Axioms and go insert them in scarab printers.
Scarabs get to Epsilon and make him OP.

Righteousness, Ever-Upwards
Left-Pawed Paths, Sinking Down

**Everyone Is Legendary**

* second rewrite lore to feed others. Also their contin should be on turnstart.
* Make it possible to pass the time when dead and only respawn when you truly want it. Useful with felidol passive 
* put question marks in the craft recipe indicator if you don't know that one yet (say, while looking at an enemy)
- Lore is that Terminal needs vessels that believes in those caste-respective axioms so he doesn't brainwash himself.

**Brushstroke Crafting**

* Become able to place a whole pattern in one shot by selecting from catalogue

**"Serene Collective" design**

Glamour injectors to safely view the Collective without being infected.
Sell your memories (nodes) to the Collective to cleanse yourself of infection.
Make creatures be able to be "injected" with serenes to harmonize them as a non-violent way of defeating enemies.
The infection starts with "drawbridge unauthorized access" while you are in a Vision.
The reason the player can't attack the intruder is because spells only work against those who believe in them.
Serene components should force the whole axiom to be serene.
Using a serene soul directly should imply some form of risk reward.
The soul quarry is "unrealized potential" to the harmony
Each death/failure makes the harmony wonder if you are making the challenges hard intentionally to betray them, which makes them spread the cyan light inside you to ensure your loyalty.
Harmonists can only be hurt by axioms no melee attacks

* Soul Press - convert 10 turbulent into 1 disciplined & 9 waste "pick off the bits and pieces that are primed for submission from the lot"

Universal laws on tiles, not enemies, who have their own Petrified souls

**Contingencies**

* On death: revives you if positive HP at the end of the spell. that latter part is kind of op, maybe make it a serene mutator that lets you heal at 0 hp?
* Prevent infinite loops (on tele, blink) in some way

**Functions**

* A recall point/teleport across layers.
* A soul that occasionally gives difficult "Simon Says" rules to follow. The player is rewarded for not transgressing.
* Devour: all partially damaged targets are executed.

**Forms**

* Random target with fat power. Feral.
* something that travels across every adjacent wall and adjacent floors to those walls
* orbital laser form, hits 50 % whole screen in stripes

**Mutators**

* Increase power depending on influence?
* Get random power up or down.
* Get power for damaged units (not at max hp) shroud of silent mist style
* Extra power if includes a function with a status effect. Status effect becomes random.
* Make effect appliers cleanse that effect instead
* A mutator that nullifies spellcraft. OR JUST DON'T CLEAR THE WHOLE FLOOR
* Serene Concordance: massive power if all components are of the same caste or serene. even bigger if only serene

* NOT: inverts all targets, what was now isn't and what wasn't now is

**Research Nodes**
Terminal is made out of shattered souls, that is why he can shapelessly assume the shape of others?

The Crimson Forest - Anisychia research

An Enticing Song - I have heard something out of this world...
In Unity We Flourish

Form - epsilon, all 4 previous tiles you stood on are targets

The Self is modular, the player is but one of Terminal's many facets. 

Offer bonuses for having more serene stuff at a time?

* Serene Souls should change their name with each added Serene Soul into increasingly positive titles.

**Epsilon Rework**

* Psychodrones risk being grindy if they let you farm Ordered souls. They should instead give you a status that turns all your spells to ordered type.
* Naia is too OP vs Epsilon. Make Epsilon knockback the player and gain invulnerability if he loses more than 33% of his HP in a single turn.
* In fact, Epsilon needs buffs in general: make the dash leave behind flames, make the lasers do damage.

**Names of things**

World Seed

Epsilon - Ordered champion
Senet - Vile champion

Ataixa - Unhinged Champion
Boolean - Artistic Champion

Rose - Saintly champion
Anisychia - Feral champion

some objectives

Regal Frills and Golden Yellows
Purple Taint Chokes Out Life

Flaring Orange Burns Away Restraint
One World, All Black and White

Sickly Sweet Ribbons of Pink
Bright Red Blood, A Shriek

Surging Cyan Rivers Drown Thought


**Various Contingencies**

- When moving up... (3 Saintly)
- When moving down... (3 Vile)
- When moving left... (3 Unhinged)
- When moving... (5 Feral)
- When moving in a circle... (1 Artistic)
- When breathing a Soul... (3 Saintly)
- After using all six Castes (without repeats)... (1 Random)
- When a turn passes while having 5 or less Value remaining... (1 Unhinged)
- When reshuffle deck...

**Low priority nitpicks**

* Potential softlock with Apis constrict + him dying after applying his constrict
* Epsilon's vulnerability markings cause confusion.
* Double speed enemies still seem to occasionally return to their previous tile.

**UI**

* Inspect elements in the sidebar to know what they do.
* Add tile abilties. Like "opens on room clear". Maybe make returnExits different too.
* Cool sliding animation in Inventory/Modules.

**Rooms & Creatures**

* The Vile Ideology Pole should be full of Felidols of all flavours, always immobile. There are conveyor belts and water currents to push the toys/statues around to stop them from being extremely overpowered despite never actually moving.
* Roseic gas should interact more directly with inhaling, and give a lower glamour score if only standing in it.
* Add more Epsilon cores to vary the runs. Massive tongue-whip with interesting pathfinding, shockwave that causes kerosene explosions.
* a gardener room that is completely peaceful but gets ridiculously hectic if the player dares step on a flower.
* A peaceful lake that becomes sinister while using roseic toxin.
* A massive enemy over 9 tiles, composed of combined textures. Maybe in the Spire. Alpha, Ordered champion?
* A joke room where a Harmonizer insists on the purity and the Sameness of the roguelike genre. Mention that all roguelikes are theoretically real-time due to the player having a limited mortal lifespan, which the Harmony is exempt from. Only the Harmony can play true roguelikes.
* Sugcha statues, that cause psychic damage when broken.
* Pressurized glamorous toxin behind breakable glass that spreads through the room.
* Turn anisychia into an anxiety-flavoured encounter with phobias like claustrophobia or vertigo, reflavour Oracles.
* And make it print out tons of red and bold text very fast panic zealot style.
* A rooftop level with walking on walls.
* A funny moment if you turn yourself into a Felidol and get softlocked into an eternal staring contest with another Felidol. Maybe the stare has some paralyzing property...

* A radioactive vox area that randomly mutates souls, for better or worse.

**Lore & Writing**

* Harmonizers are invincible and ethereal outside the spire, because they can only harm their own. In the Spire you have a bit more power over them, as the influence of the environment makes you a little bit more like them.
* Make it clearer why enemies attack the player - they are desperate for a ride and are also testing to see if Terminal is their true saviour. Maybe add dialogue upon entering random rooms that highlights this. Clarify that the reason enemies are weak and never back down is because they *want* to die to Terminal to board the train to the Next World.
* The reason the game is in glyphs is because it must be "censored" as some creatures are cognitohazardous. The Harmonizers/Roseic Servants in particular.
* Zaint's philosophy is apathy and detatchment. This makes sense, because altruism has no place in a world where literally everyone is a god with unlimited resources.
* Also his name is just zaint because he started with a Z, but never finished out of laziness and just kept calling himself "saint" after his noble title.
* If Fluffy is Pride incarnate, Terminal is Envy incarnate - gnawing, coveting value. It is only right that it would be put against a being of infinite value...
* Fluffy is an empty shell, if defeated she may be replaced and duplicate you.
* The Harmony contains "ultimate lifeforms", as they have reached maximum complexity.
* Fluffy should taunt you about how your task is endless and infinite, and how submission to the Harmony is the only sensible course of action.
* “It is not death we distribute, it is uplifting. All ruffians of this world contain a primordial Fluffy - all we do is permutation of the switches in incorrect positions, while leaving the correct ones in place. Some are more Fluffy than others before our intervention […]” Therefore, the more (narcisstic) you are, the more vulnerable you become.

* Terminal is caged because he needs to be the observer of the whole world for it to continue ticking.

**Insane/overly ambitious stuff**

*absolute madness below*

* Rose's ability to "drown out in endless lives".
* Give Rose something about choked souls, drowned souls, the player's own soul still existing but with piles upon piles of roseic souls on top preventing access to who they once were

* The "Next World" is the world above, and all the Old World data is Higher Terminal's dream.
* The very first run of the game, you play as the first Terminal to ever encounter Fluffy, who spawns from Glamorous Toxin. She is not directly with you and spawns all the way in the Roseic Circus after a while, so you have time to learn the game.
* After you fail and the world is Harmonized, the Terminal in the world above wakes up from a terrible dream of Harmony, turning him into a carrier.
* Fluffy continues to harmonize infinite recursive worlds one by one, until the player finally stops it.
* This causes the world to go back to the "first run" model. At this point, you must rush the Roseic Circus before Fluffy ever becomes a thing to cut off the cycle once and for all.
* Then, you must remain behind and let yourself be consumed to avoid the idea of Fluffy being transmitted to the upper realms, to truly end the spread.
* Possibly offer an alternative to forcepush Fluffy across all higher levels and also end the spread... by having nothing left to spread to.

* Inhalable Glamorous Toxin that transforms the current room into a hallucinatory version. Maybe Rose's soul does this.
* Rose himself should be found at the very bottom of hallucinations within hallucinations.
* The final boss being inspired by M. Industries. Inside Ronin's spaceship (built out of negentropic force), farm tokens on 4 plates while being assailed by the Harmony, and buy upgrades in the form of pushable boxes to give more firepower to Ronin's squad.
* Ronin's weapon should be a big-ass war vacuum.
* Simple pvp duels with components loadouts?
* After all, if an image is worth a thousand words, surely a video must be worth a million. Maybe put regular game footage in generic trailer format, with the Harmony subtly replacing elements here and there, or even pausing the music and showing off one of their rooms before the video switches to something else like nothing happened. Have them take over the footage progressively.
* Bringing the Herald all the way to the end of the game should result in something funny.
* A Zaint-themed room that makes you forgo ever getting achievements to cleanse your mind of external motivation.
* Zaint should open a joke metaprogression shop that does something ridiculously insignificant, like 1/1 million chance of gaining 1 bonus damage per hit or changing the colour of something in the GUI.

**To use later**

*"A glass canister filled with wispy, pink vapour. The label reads, 'Once the fluorescent tubes' buzzing becomes unbearable, break immediately and inhale contents. May induce ego death.'"*

- Saints' Breath description

**Banishment Zone (unused/dropped, but fun to look at)**

**Initial "build your dungeon" idea**

* Start off in World Seed. Do the tutorial.
* Generate Souls by "dancing" with Zaint while the Harmony tries to get inside.
* Build your dungeon from Souls forced to dream about the Old World's past. It has to be easy enough for you to win, but hard enough to stop the Harmony from chasing you too fast.
* Escape into the dungeon and beat the boss to prove your worth. From the boss chamber, trade with the boss and rebuild to escape into another layer.
* Entering contact with the Harmony may cause them to start infecting created worlds from the inside... allowing options to trade for forbidden tech.
* Upon becoming OP enough, ascend back through the Serene Spire all the way to the topmost level and destroy the infection at the root to win.

**Map Variety Project**

* An image of a huge twisting maze, like brain coral. Coloured dots can be clicked to enlarge portions of Faith's End, corresponding to saved World objects.
* Actually, could the most interesting landmarks on the map be marked, and could the player control their respawning to approach them?
* The souls you sacrifice on death determine your respawn direction - towards an "Ideology pole" that contains an exit point, one for each caste. Should these "poles" expand as the game progresses, and cause a cataclysm if they ever touch the centre? This could be a nice replacement for "hunger clock" type mechanics.

The dungeon branch that is currently known as the "Roseic Circus" will be *merged* with Faith's End, and will be made accessible only through the inhalation of the mysterious psychedelic toxin already used by some of the game's enemies - Saints' Breath. This should encourage the player to clear a few rooms, toke some of "that sweet pink stuff", and then *retrace their steps* as they now have a new series of hallucinatory creatures to fend off in these normally deserted rooms, with appropriate psychic-themed loot.

I'm really excited about this plan and the progress done this week, and I look forward to deepening this concept!

There is a purpose to this madness beyond simple atmosphere. Some [interesting waypoints]() are marked on Faith's End, but are technically unreachable through walking as they are located literal thousands of rooms away. This is where the protagonist's signature ability comes in - limited, but controlled reincarnation. The location where the game will continue following a non-final death can be *directed* according to which Souls the player chooses to sacrifice upon meeting their demise, and thus skip across megakilometres of concrete in the goal of exploring these mysterious landmarks. This furthens my game's design goal of "turning mistakes into tools for success".

kind of sad all that epic code was thrown away but it is what it is

**Initial Serene mechanic idea**

* Across their encounters with rooms featuring Harmony representatives, the player may be granted the opportunity to equip a Serene Soul, one of the possible Legendary souls that can reign over and alter the effects of one of the six Castes. Unlike other Legendaries, these can go into any Caste slot.

* Spells modified by the Serene Soul gain a very powerful conversion touch ability, which oneshots one enemy and turns them into an allied Harmonizer to assist the player in clearing the room. 

* However, each usage of this power causes a random Soul to be converted into the Caste in which the Serene Soul has taken root. Not necessarily that bad - you'll just get to use this awesome spell more often! Additionally, the player unlocks a new Harmonic Modulator slot for each Serene Soul they have incarnated.

* Harmonic Modulators are glass spheres containing an odd gaseous cybernetic. Should the player discover one of these rare trinkets, they may inhale the vapour to gain a new toggleable ability - usually some useful passive such as flight, combat actions taking less turns, or additional movement options. Each one also passively grants extra buffs to allied Harmonizers, like also giving them a conversion touch ability. Infectious! I have the code for a few of these already written at the time of posting this.

* To continue using an active Modulator, Souls from the player's repertoire will keep getting converted into the Caste the Serene Soul has taken root in, similarly to the Serene Soul's normal active effect. At this state, the risk of ending up with all your Souls belonging a single Caste can become a problem, but all it takes is some discipline to avoid using Modulators if they are not required for the current challenge. With the right temperance, using the Harmony's arsenal could become a fantastic asset to your character's potential!

* Should you go overboard and use one of these effects when there are no Souls left to convert, the Serene Soul will duplicate, and take root into *another* Caste, deepening your grave even further. 

* Serene Souls cannot be removed through normal means.

* Thus, the undisciplined player will find their character becoming corrupted from the inside by their own inventory, at an exponential rate that should prove difficult to stop. Desperate for a cure, they may become the victim of swindlers across the dungeon charging exorbitant prices - paid in Souls, of course - for pruning Serene Souls.

* Should the player have all 6 of their Castes infested, then the Harmony will have succeeded in its devious plan, and enlisted you to join their side. At this point, I'd like the game to continue with a kit fully reliant on summoning extremely buffed-up Harmonizers, and to potentially change the game's ending if I were to ever make a proper one.


# Controlled Opposition

It was only a matter of time with the way my development was headed, but the player can now craft their own enemies. The UI for this is far from complete, but the code is there.

Saying it out loud sounds insane (and it very well might be), but it's more straightforward than it appears. Every TGFP enemy is a blank slate with some HP points and an AI that tells it to move and bump attack. That's it. All their special abilities come from the Axioms implanted in them, which create all kinds of programming-inspired logic blocks: "When taking a step, on self, stop for one turn, then place down a trap which, when stepped on, paralyses the creature that stepped on it".

The player was already crafting their own Axioms to cast spells. You might see where I am going with this: just let the player whip up some creative Axioms, implant those in a basic enemy, then spread clones of this new creature across the dungeon.

In this case, why not just create some hilariously suicidal creatures with thought-provoking Axioms such as "do nothing, then perish" or "heal your enemies every millisecond", then proceed to win the game without a trace of challenge? 

This ties in with the Harmony core mechanic - making creatures attack other things than the player is already a feature, through the charm/summoning magic effects. My idea would be that some Harmonic agents pursue you across the dungeon, and are slowed down by the enemies you have triumphed over, now respawned and working for you. If you only make utter weaklings, the Harmony will easily power through the opposition and reach your position in little time. If you make death machines, well, you will be the one to enter a die-and-retry loop while the Harmony spawn-camps you.

If you are losing your speed, you should be getting one of two things: either damage huge enough to delete everything before you are oneshotted, or defense huge enough to not be a cat anymore.

Beast Form is good enough to stop D:1-2 splats but quickly should be replaced by ~~Chernobyl~~ Flux Form, which does obscene damage without sacrificing your speed. Manifold Assault applies the radioactive marks remotely, which is hilarious.

Since Blade ~~Hands~~ Paws lost its spellcasting hindrance, it is now the logical upgrade to Flux Form for even more damage.

Maw is passable. The lifesteal is appreciated but Bl